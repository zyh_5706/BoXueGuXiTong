from django.conf.urls import url
from . import views

urlpatterns=[
    url('^orglist',views.OrgListView.as_view(),name='org_list'),
    url('^orgteacher',views.TeacherListView.as_view(),name='teacher_list'),
    url('^add_fav',views.AddfavView.as_view(),name='add_fav'),
]