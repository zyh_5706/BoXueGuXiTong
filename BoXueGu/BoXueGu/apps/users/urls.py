from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^register/$', views.RegisterView.as_view(), name='register'),
    url(r'^forget/$', views.ForgetPwdView.as_view(), name='forget_pwd'),
    url(r'^user_info/$', views.UserInfoView.as_view(), name='user_info'),
    url(r'^mymessage/$', views.Mymessage.as_view(), name='mymessage'),
    url(r'^mycourse/$', views.Mycourse.as_view(), name='mycourse'),
    url(r'^myfav_org/$', views.Myfav_org.as_view(), name='myfav_org'),
    url(r'^image_upload/$', views.Image_upload.as_view(), name='image_upload'),
    url(r'^reset/(?P<active_code>.*)/$', views.ResetView.as_view(), name='reset_pwd'),
    url(r'^modify_pwd/$',views.ModifyPwdView.as_view(),name='modify_pwd'),
]

