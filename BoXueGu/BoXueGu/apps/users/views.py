from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password
from django.shortcuts import render, redirect
from django.views import View
from celery_tasks.email_active.tasks import send_active_mail

from BoXueGu.utils import boxuegu_signature
from users import constants
from users.models import UserProfile
from .forms import RegisterForm, LoginForm, ForgetForm, ModifyForm


class LoginView(View):
    def get(self, request):
        return render(request, 'login.html')

    def post(self, request):
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            cd = login_form.cleaned_data
            user = authenticate(request, username=cd['username'], password=cd['password'])
            if user is None:
                login_form.errors['username'] = '用户名或密码错误'
                return render(request, 'login.html', {'form_errors': login_form.errors})
            else:
                login(request, user)
                # return HttpResponsePermanentRedirect(reverse('contents:index'))
                return redirect('/')
        else:
            return render(request, 'login.html', {'form_errors': login_form.errors})


class LogoutView(View):
    def get(self, request):
        # 删除状态保持
        logout(request)
        response = redirect('/login/')
        return response


class RegisterView(View):
    def get(self, request):
        register_form = RegisterForm()
        context = {
            'register_form': register_form,
        }
        return render(request, 'register.html', context)

    # def post(self, request):
    #     """
    #         验证表单数据
    #     :param request:
    #     :return:
    #     """
    #     # 1、获取前端传递的表单数据
    #     data = request.POST
    #     # 2、验证表单数据
    #     register_form = RegisterForm(request.POST)
    #     res = register_form.is_valid()  # 验证成功返回True，验证失败返回False
    #     # res = True
    #     if res:
    #         # 验证成功，则执行相应业务逻辑操作，这里就直接返回验证成功后的字段数据
    #         # 获取验证成功后的字段
    #
    #         # 取出email和password
    #         email = request.POST.get("email", "")
    #
    #         # 判断邮箱是否重复
    #         try:
    #             user = UserProfile.objects.get(email=email)
    #         except Exception as e:
    #             user = None
    #
    #         if user:
    #             return render(request, 'register.html', {'register_form': register_form})
    #         mobile = request.POST.get("mobile", "")
    #
    #         # 判断手机号是否重复
    #         try:
    #             user = UserProfile.objects.get(mobile=mobile)
    #         except Exception as e:
    #             user = None
    #
    #         if user:
    #             return render(request, 'register.html', {'register_form': register_form})
    #         pass_word = request.POST.get("password", "")
    #         # 实例化用户，然后赋值
    #         user_profile = UserProfile()
    #         user_profile.email = user_name
    #         user_profile.username = user_name
    #         user_profile.mobile = mobile
    #         # 将明文转换为密文赋给password
    #         user_profile.password = make_password(pass_word)
    #         user_profile.save()  # 保存到数据库
    #         return render(request, "login.html")
    #     else:
    #         # form表单验证失败，将错误信息传给前端
    #         return render(request, "register.html", {"register_form": register_form})
    #
    #     # 验证失败，则在注册模板中通过register_form.errors获取错误
    #     return render(request, 'register.html', {'register_form': register_form})
    def post(self, request):
        register_form = RegisterForm(request.POST)
        res = register_form.is_valid()
        if res:
            username = register_form.cleaned_data['email']
            # 判断用户名是否重复
            if UserProfile.objects.filter(email=username).count() > 0:
                register_form.errors['email'] = '邮箱已存在'
                return render(request, 'register.html', {'register_form': register_form})
            # 判断手机号是否重复
            mobile = register_form.cleaned_data['mobile']
            if UserProfile.objects.filter(mobile=mobile).count() > 0:
                register_form.errors['email'] = '手机号已存在'
                return render(request, 'register.html', {'register_form': register_form})
            password = register_form.cleaned_data['password']
            nickname = register_form.cleaned_data['nickname']
            # 保存邮箱，密码，手机号
            user = UserProfile.objects.create_user(
                username=username,
                email=username,
                password=password,
                nick_name=nickname,
                mobile=mobile
            )
            # 状态保持
            login(request, user)
            return redirect('/')
        else:
            return render(request, 'register.html', {'register_form': register_form})


class ForgetPwdView(View):
    def get(self, request):
        forget_form = ForgetForm()
        context = {'forget_form': forget_form}
        return render(request, 'forgetpwd.html', context)

    def post(self, request):
        forget_form = ForgetForm(request.POST)
        res = forget_form.is_valid()
        if res:
            email = forget_form.cleaned_data['email']
            # 判断邮箱正确
            try:
                UserProfile.objects.get(email=email)
                # 发邮件：耗时代码，使用celery异步
                # 将用户编号加密
                token = boxuegu_signature.dumps({'email': email}, constants.EMAIL_ACTIVE_EXPIRES)
                # 拼接激活的链接地址
                verify_url = settings.EMAIL_VERIFY_URL + token
                # 异步发邮件
                send_active_mail.delay(email, verify_url)
                # 响应
                return render(request, 'send_success.html')
            except:
                forget_form.errors['email'] = '邮箱不存在'
                return render(request, 'forgetpwd.html', {'forget_form': forget_form})
        else:
            return render(request, 'forgetpwd.html', {'forget_form': forget_form})


class ResetView(View):
    def get(self, request, active_code):
        try:
            data = boxuegu_signature.loads(active_code, constants.EMAIL_ACTIVE_EXPIRES)
        except:
            return render(request, 'active_fail.html')
        else:
            email = data.get('email')
            try:
                UserProfile.objects.get(email=email)
            except:
                return render(request, 'active_fail.html')
            else:
                return render(request, 'password_reset.html', {'email': email})


class ModifyPwdView(View):
    def post(self, request):
        modify_form = ModifyForm(request.POST)
        res = modify_form.is_valid()
        if res:
            data = modify_form.cleaned_data
            pwd1 = data.get('password1')
            pwd2 = data.get('password2')
            email = data.get('email')
            if pwd1 != pwd2:
                modify_form.errors['password1'] = '密码输入不一致'
                return render(request, 'password_reset.html', {'modify_form': modify_form,'email':email})
            else:
                try:
                    user = UserProfile.objects.get(email=email)
                except:
                    return render(request, '404.html')
                else:
                    user.set_password(pwd1)
                    user.save()
                    return render(request, 'login.html')
        else:
            return render(request,'password_reset.html',{'modify_form':modify_form})

class UserInfoView(View):
    def get(self, request):
        return render(request, 'usercenter-info.html')


class Mymessage(View):
    def get(self, request):
        return render(request, 'usercenter-message.html')


class Mycourse(View):
    def get(self, request):
        return render(request, 'usercenter-mycourse.html')


class Myfav_org(View):
    def get(self, request):
        return render(request, 'usercenter-fav-org.html')


class Image_upload(View):
    def get(self, request):
        return render(request, 'usercenter-message.html')
