from captcha.fields import CaptchaField
from django import forms
from django.core import validators

from users.models import UserProfile


class RegisterForm(forms.Form):
    """
        用户注册表单
    """
    # email = forms.EmailField(max_length=30,min_length=6,required=True,label='用户名',error_messages={'required': "邮箱不能为空"})
    # password = forms.CharField(max_length=20, min_length=8, required=True,label='密码',error_messages={'required': "密码不能为空"})
    # mobile = forms.CharField(max_length=11, min_length=11,required=True,label='手机号',error_messages={'required':'手机号不能为空'})
    email = forms.CharField(validators=[
        validators.RegexValidator("^[0-9a-zA-Z_]{0,19}@[0-9a-zA-Z]{1,13}\.[com,cn,net]{1,3}$", message='请输入正确的邮箱格式！')],
                            required=True, error_messages={'required': "邮箱不能为空"})
    password = forms.CharField(validators=[
        validators.RegexValidator("^[0-9a-zA-Z_]{6,20}$", message='密码为6-20位，且不能有特殊字符！')], required=True, label='密码',
                               error_messages={'required': "密码不能为空"})
    nickname = forms.CharField(max_length=10, min_length=4, required=True, label='昵称',
                               error_messages={'required': "昵称不能为空"})
    mobile = forms.CharField(validators=[validators.RegexValidator("1[345678]\d{9}", message='请输入正确格式的手机号码！')],
                             required=True, error_messages={'required': "手机号码不能为空"})

    # 增加验证码字段
    captcha = CaptchaField(label='验证码', error_messages={'invalid': "验证码不正确"})


class LoginForm(forms.Form):
    """
        用户登录表单
    """
    username = forms.CharField(max_length=30, min_length=6, required=True, error_messages={'required': "账号不能为空"})
    password = forms.CharField(max_length=20, min_length=8, widget=forms.PasswordInput, required=True,
                               error_messages={'required': "密码不能为空"})

class ForgetForm(forms.Form):
    """
        忘记密码表单
    """
    email = forms.EmailField(max_length=30, min_length=6, required=True, error_messages={'required': "账号不能为空"})
    captcha = CaptchaField(label='验证码', error_messages={'invalid': "验证码不正确"})

class ModifyForm(forms.Form):
    """
        修改密码表单
    """
    password1 = forms.CharField(validators=[
        validators.RegexValidator("^[0-9a-zA-Z_]{6,20}$", message='密码为6-20位，且不能有特殊字符！')], required=True, label='密码',
                               error_messages={'required': "密码不能为空"})
    password2 = forms.CharField(required=True, label='密码',error_messages={'required': "密码不能为空"})
    email = forms.CharField(validators=[
        validators.RegexValidator("^[0-9a-zA-Z_]{0,19}@[0-9a-zA-Z]{1,13}\.[com,cn,net]{1,3}$", message='请输入正确的邮箱格式！')],
        required=True, error_messages={'required': "邮箱不能为空"})
    # captcha = CaptchaField(label='验证码', error_messages={'invalid': "验证码不正确"})