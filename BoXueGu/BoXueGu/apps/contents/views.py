from django.shortcuts import render
from django.views import View

from courses.models import Course, BannerCourse
from organization.models import CourseOrg
from users.models import Banner

class IndexView(View):
    def get(self, request):
        all_banners = Banner.objects.all()
        courses = Course.objects.all()
        banner_courses = BannerCourse.objects.all()
        course_orgs = CourseOrg.objects.all()

        content = {
            'all_banners': all_banners,
            'courses': courses,
            'banner_courses': banner_courses,
            'course_orgs': course_orgs,
        }
        return render(request, 'index.html',content)


class UserInfoView(View):
    pass
