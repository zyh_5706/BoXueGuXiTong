from django.conf.urls import url
from . import views

urlpatterns=[
    url('^courseslist/$',views.CourseListView.as_view(),name='course_list'),
]