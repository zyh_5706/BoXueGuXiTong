from django.contrib.auth.backends import ModelBackend
from users.models import UserProfile


class BoXueGuModelBackend(ModelBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):

        # 判断是否是后台登录
        print(request)
        if request is None:
            try:
                user = UserProfile.objects.get(username=username, is_staff=True)
            except:
                user = None
            if user is not None and user.check_password(password):
                return user
        else:
            try:
                user = UserProfile.objects.get(email=username)
            except:
                # 如果未查到数据，则返回None，用于后续判断
                try:
                    user = UserProfile.objects.get(mobile=username)
                except:
                    return None

            # 判断密码
            if user.check_password(password):
                return user
            else:
                 return None
